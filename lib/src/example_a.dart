
import 'dart:math';

///
/// example.dart's description
///
/// Dartコードの動きを確かめる用のDartファイル。
///
/// 起動コマンド
/// dart example.dart
///
///
class Logger {
  final String name;

  factory Logger(String name) {
    return Logger._internal(name);
  }

  Logger._internal(this.name);
  Logger.from({this.name = 'ddd'}) {
    print('from');
    //this.name = text;
    //return Logger._internal(text);
  }
}

enum Color {
    red,
    green,
    blue,
}

void main() {
  var l = Logger('aaa');
  print(l.name);

  var lo = Logger._internal('bbb');
  print(lo.name);

  var logg = Logger.from();
  print(logg.name);

  var numbers = RegExp(r'\d+');
  print(numbers);
  print(numbers.toString());

  var allCharacters = 'llamas live fifteen to twenty years';
  var someDigits = 'llamas live 15 to 20 years';

  print(!allCharacters.contains(numbers));
  print(allCharacters.contains(numbers));
  print(someDigits.contains(numbers));
  print(numbers.hasMatch(someDigits));

  for (var match in numbers.allMatches(someDigits)) {
    print(match.group(0)); // 15, then 20
  }

  var str = 'Parse my string';
  var str2 =
      'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

  var base62 = RegExp(r'(\w+)');
  //RegExp base62 = RegExp(r'[a-zA-Z0-9]');

  var matches = base62.allMatches(str);
  for (var m in matches) {
    print(m.group(0));
  }
  print(matches.first);
  //print(matches.first.firstMatch);

  var matches2 = base62.allMatches(str2);
  for (var m in matches2) {
    print(m.group(0));
    //print(m.group(0).length);
  }
  print(base62.firstMatch(str2)!.group(0));

  print(base62);

  var num = Random().nextInt(62);
  print(num);

  print( str2.substring(61,62) );

  // Enum
  print(Color.red);
  print(Color.red.toString());
  print(Color.red.toString().split('.'));
  print(Color.red.toString().split('.')[1]);

  // 時刻表記
  print('DateTime.now() = ${DateTime.now()}');
  print('DateTime.now().toIso8601String() = ${DateTime.now().toIso8601String()}');
  print('DateTime.now().millisecondsSinceEpoch = ${DateTime.now().millisecondsSinceEpoch}');
  print('DateTime.now().day = ${DateTime.now().day}');
  print('DateTime.now().year = ${DateTime.now().year}');
  print('DateTime.now().month = ${DateTime.now().month}');
  print('DateTime.now().millisecond = ${DateTime.now().millisecond}');
  print('DateTime.now().timeZoneName = ${DateTime.now().timeZoneName}');
  print('DateTime.now().timeZoneOffset = ${DateTime.now().timeZoneOffset}');
  print('DateTime.now().isUtc = ${DateTime.now().isUtc}');
  print('DateTime.now().weekday = ${DateTime.now().weekday}');
  print('DateTime.now().second = ${DateTime.now().second}');
  print('DateTime.now().hour = ${DateTime.now().hour}');
  print('DateTime.now().minute = ${DateTime.now().minute}');
  print('DateTime.now().hashCode = ${DateTime.now().hashCode}');
  print('DateTime.now().microsecond = ${DateTime.now().microsecond}');
  print('DateTime.now().microsecondsSinceEpoch = ${DateTime.now().microsecondsSinceEpoch}');

}