
import 'package:colorx/src/colorx_base.dart';
import 'package:pub_base/base.dart';
// import 'package:tint/tint.dart';
// import 'package:palestine_console/palestine_console.dart';

/// 
/// this is example class's documentation comment.
/// 
abstract class Example
    extends
        Subject
    with
        A
    implements
        Setting
        ,Signal
{

    final a = 'aaa';

    String get b => 'bbb';

    /// this is Example class constructor's documentation comment.
    Example() {
        excute();
    }

    Example.from() {}

    /// this is excute()'s documetation comment.
    void excute();

    /// this is printm()'s documetation comment.
    void printm(Object object) {
        final a = '';
        print('$runtimeType: '.brightBlue.bold + object.toString());
    }

    void printv(String name, Object? value, [String? a]) {
        // print('$runtimeType: '.brightBlue.bold + '$name'.gray + ' = '.gray + value.toString());
        print('$runtimeType: '.brightBlue.bold + '$name'.brightBlue + ' = '.brightBlue + value.toString());
    }

}

mixin A {}
