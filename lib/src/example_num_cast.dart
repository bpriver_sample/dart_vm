/// 
/// 今のところ没内容 20220112時点
/// 保証型と実行時型の num,int,doubleでのcastのやつ
/// 
void main() {

    // 詳細に言えば、 Error ではなく、Exception である。
    try {
        print('Case: var value = 0;');
        var value = 0;
        print('value type: ${value.runtimeType}');
        value as double;
    } catch (e,s) {
        print('Error Message: $e,${s}');
        print('');
    }

    try {
        print('Case: int value = 0;');
        int value = 0;
        print('value type: ${value.runtimeType}');
        value as double;
    } catch (e) {
        print('Error Message: $e');
        print('');
    }

    try {
        print('Case: num value = 0;');
        num value = 0;
        print('value type: ${value.runtimeType}');
        value as double;
    } catch (e) {
        print('Error Message: $e');
        print('');
    }

    try {
        print('Case: num value = 0.0;');
        num value = 0.0;
        print('value type: ${value.runtimeType}');
        print('');
    } catch (e) {
    }

    try {
        print('Case:');
        print('    num value = 0.0;');
        print('    value as num;');
        num value = 0.0;
        value as num;
        value;
        value as int;
        value;
    } catch (e) {
        print('Error Message: $e');
        print('');
    }

    try {
        print('Case: num get value => 0');
        Sample.value as double;
    } catch (e) {
        print('value type: ${Sample.value.runtimeType}');
        print('Error Message: $e');
        print(''); 
    }

    toIntAndToDouble();

}

class Sample {
    static num get value => 0;
}

void toIntAndToDouble() {

    var value = 0.5;
    print(value.toInt());
    print(value.toInt().runtimeType);
    
    var value2 = 0;
    print(value2.toDouble());
    print(value2.toDouble().runtimeType);

}