
import 'package:args/args.dart';
import 'package:dart_vm/dart_vm.dart';

// Write command-line apps | Dart: "https://dart.dev/tutorials/server/cmdline#setting-exit-codes"
// args | Dart Package: "https://pub.dev/packages/args/example"
// args - Dart API docs: "https://pub.dev/documentation/args/latest/"
class ArgsExample
    extends
        Example
{

    static const flagAName = 'flaga';
    static const flagBName = 'flagb';
    static const flagCName = 'flagc';
    static const optionAName = 'optiona';
    static const optionBName = 'optionb';
    static const multiOptionAName = 'multioptiona';
    static const commandAName = 'commanda';
    static const commandBName = 'commandb';

    static final argParser = ArgParser()
        // addFlag method - ArgParser extension - args library - Dart API: "https://pub.dev/documentation/args/latest/args/ArgParser/addFlag.html"
        // 省略形
        //      -a
        // 非省略形
        //      --flaga
        ..addFlag(
            flagAName,
            negatable: false,
            abbr: 'a',
            help: 'this is help of flag a.',
            defaultsTo: false,
        )
        ..addFlag(
            flagBName,
            negatable: true,
            abbr: 'b'
        )
        // addOption method - ArgParser extension - args library - Dart API: "https://pub.dev/documentation/args/latest/args/ArgParser/addOption.html"
        ..addOption(
            optionAName,
            abbr: 'd',
            help: 'this is help of option a.',
            valueHelp: 'this is value help of option a.'
        )
        ..addMultiOption(
            multiOptionAName,
            abbr: 'e',
            help: 'this is help of multi option a.',
            // splitCommas が true なら
            //「-e actor,name,xxx」のように コンマで区切ることで 複数の parameter を渡すことができる.
            splitCommas: true,
        )
        // addCommand method - ArgParser extension - args library - Dart API: "https://pub.dev/documentation/args/latest/args/ArgParser/addCommand.html"
        ..addCommand(
            commandAName,
            ArgParser()
                ..addFlag(
                    flagCName,
                    abbr: 'c',
                    help: 'this is help of flag c.',
                )
                ..addCommand(
                    commandBName,
                    ArgParser()
                        ..addOption(
                            optionBName,
                            abbr: 'f',
                        )
                )
        )
        // addSeparator method - ArgParser extension - args library - Dart API: "https://pub.dev/documentation/args/latest/args/ArgParser/addSeparator.html"
        ..addSeparator(
            '--------------'
        )
    ;

    ArgsExample(this.arguments);

    final List<String> arguments;

    @override
    void excute() {

        printv('argParser', argParser);
        printv('argParser.allowTrailingOptions', argParser.allowTrailingOptions);
        printv('argParser.allowsAnything', argParser.allowsAnything);
        printv('argParser.commands', argParser.commands);
        printv('argParser.options', argParser.options);
        printv('argParser.usage', argParser.usage);
        printv('argParser.usageLineLength', argParser.usageLineLength);
        printv('argParser.toString()', argParser.toString());
        printv('argParser.findByAbbreviation(\'a\')', argParser.findByAbbreviation('a'));
        printv('argParser.findByAbbreviation(\'xxx\')', argParser.findByAbbreviation('xxx'));
        printv('argParser.findByNameOrAlias(\'b\')', argParser.findByNameOrAlias('b'));
        printv('argParser.findByNameOrAlias(\'flagb\')', argParser.findByNameOrAlias('flagb'));
        printv('argParser.findByNameOrAlias(\'yyy\')', argParser.findByNameOrAlias('yyy'));

        final ArgResults argResults = argParser.parse(arguments);

        printv('argResults', argResults);
        printv('argResults.rest', argResults.rest);
        printv('argResults.arguments', argResults.arguments);
        printv('argResults.command', argResults.command);
        printv('argResults.command?.rest', argResults.command?.rest);
        printv('argResults.command?.name', argResults.command?.name);
        printv('argResults.command?[flagCName]', argResults.command?[flagCName]);
        printv('argResults.command?.command', argResults.command?.command);
        printv('argResults.command?.command?[optionBName]', argResults.command?.command?[optionBName]);
        // printv('argResults.?[commandAName]', argResults.command);
        printv('argResults.name', argResults.name);
        printv('argResults.options', argResults.options);
        printv('argResults.wasParsed($flagBName)', argResults.wasParsed(flagBName));
        printv('argResults[$flagAName]', argResults[flagAName]);
        printv('argResults[$flagBName]', argResults[flagBName]);
        // printv('argResults[$flagCName]', argResults[flagCName]);
        printv('argResults[$optionAName]', argResults[optionAName]);
        // printv('argResults[$optionBName]', argResults[optionBName]);
        printv('argResults[$multiOptionAName]', argResults[multiOptionAName]);
        // printv('argResults[$commandAName]', argResults[commandAName]);
        // printv('argResults[$commandBName]', argResults[commandBName]);

        // final ArgResults argResultsC = argParserC.parse(arguments);

    }

}
