
import 'package:yaml/yaml.dart';

void main() {

    var doc = loadYaml("YAML: []") as Map;
    print(doc['YAML']);

    final a = [];

    // final b = a as List<String>; error
    
    // final c = a as List<Object>; error
    
    final d = a as List;

    final e = doc['YAML'] as List;

    final f = doc['YAML'] as List<dynamic>;
    
    // final g = doc['YAML'] as List<Object>; error

    print('end');

}