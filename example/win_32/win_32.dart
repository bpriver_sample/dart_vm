
import 'dart:ffi';

import 'package:dart_vm/dart_vm.dart';
import 'package:ffi/ffi.dart';
import 'package:win32/win32.dart';

import 'virtual_key_codes.dart';
// import 'dw_flags.dart';

/// 
/// FindWindow: "http://chokuto.ifdef.jp/advanced/function/FindWindow.html"
/// FindWindow function - win32 library - Dart API: "https://pub.dev/documentation/win32/latest/win32/FindWindow.html"
/// 
/// ウィンドウハンドルの取得に成功すると、その値が int で返る。
/// 失敗すると 0 が返る。
/// 
/// lpClassName
/// 
/// lpWindowName
///     完全一致でなければ、window handle の取得に失敗し、0 が返る。
/// 
class ExampleFindWindow extends Example {
    @override void excute() {
        printm('ウィンドウハンドルの取得に成功すると、その値が int で返る。');
        printm('失敗すると 0 が返る。');
        printm('');

        printm('window title を指定し、window handle を取得する例');
        printm('※ エクスプローラーを立ち上げた状態にしておく必要がある。立ち上げ後、何かしらの folder へ移動すると window title が変化するため 0 が返ってしまう。');
        printm('');

        printm('完全一致');
        printv('FindWindow(nullptr, TEXT("エクスプローラー"))', FindWindow(nullptr, TEXT('エクスプローラー')));
        printm('');

        printm('不完全一致');
        printv('FindWindow(nullptr, TEXT("エクスプローラ"))', FindWindow(nullptr, TEXT('エクスプローラ')));
        printm('');
    }
}

/// 
/// EnumWindows function - win32 library - Dart API: "https://pub.dev/documentation/win32/latest/win32/EnumWindows.html"
/// win32/window.dart at main · timsneath/win32 · GitHub: "https://github.com/timsneath/win32/blob/main/example/window.dart"
/// 
class ExampleEnumWindows extends Example {

    // Callback for each window found
    static int enumWindowsProc(int hWnd, int lParam) {
        // Don't enumerate windows unless they are marked as WS_VISIBLE
        if (IsWindowVisible(hWnd) == FALSE) return TRUE;

        final length = GetWindowTextLength(hWnd);
        if (length == 0) {
            return TRUE;
        }

        final buffer = wsalloc(length + 1);
        GetWindowText(hWnd, buffer, length + 1);
        print('ExampleEnumWindows: hWnd $hWnd: ${buffer.toDartString()}');
        // print('ExampleEnumWindows: window handle = $hWnd, window title = ${buffer.toDartString()}');
        free(buffer);

        return TRUE;
    }

    @override void excute() {
        printm('すべての top level window(parent を持たない window) を列挙する');
        printm('実際には、EnumWindows に渡した callback function に列挙される window の handle が渡され、その分だけ、window handle を引数に、callback function が繰り返し実行される。');
        printm('callback function が false を返すか、すべての top level window が列挙されるまで繰り返される。');
        printm('callback function は static でなければならない。');
        printm('');

        final wndProc = Pointer.fromFunction<EnumWindowsProc>(enumWindowsProc, 0);
        EnumWindows(wndProc, 0);
        printm('');

    }
}

/// 
/// EnumChildWindows
///     child window を列挙する
///     
/// 

/// 
/// ウィンドウテキスト: "http://wisdom.sakura.ne.jp/system/winapi/win32/win58.html"
/// 
/// hWnd は、対象となるウィンドウやコントロールのハンドル
/// lpString は取得したテキストを格納するための文字列バッファ
/// nMaxCount は、バッファの長さを指定します
/// バッファの長さが足りない場合、その分のテキストが切り捨てられます
///
/// 関数が成功すれば、終端文字を含まないコピーした文字列の数が
/// 失敗した場合はハンドルが無効の場合は 0 が戻り値として返ります
///
/// ウィンドウが何文字のテキスト保有しているかわからない場合
/// これだけでは、確実にテキストを取得することができません (切り捨てられる可能性がある)
/// ウィンドウテキストが何文字なのかを知るためには
/// GetWindowTextLength() 関数を使って文字数を調べます
/// 
/// 
/// 
class ExampleGetWindowText extends Example {
    @override void excute() {
        // GetWindowText(hWnd, lpString, nMaxCount);
    }
}

/// 
/// ウィンドウテキスト: "http://wisdom.sakura.ne.jp/system/winapi/win32/win58.html"
/// 
/// hWnd には、テキストの長さを調べたいウィンドウのハンドルを指定します
/// 戻り値として、ウィンドウのテキストの文字数を返します
/// ただし、 実際のテキスト長よりも多い数 になることがあります
/// 
/// これは、ANSI と Unicode の特定数の混在で起こるものなのです
/// しかし、実際のテキスト長よりも短くなることは決してないので
/// バッファを割り当てるには十分な長さを返してくれるでしょう
/// 
class ExampleGetWindowTextLength extends Example {
    @override void excute() {
        // GetWindowTextLength(hWnd);
    }
}

/// 
/// 【C++ Win32】キーボ―ド入力のすべて - とある科学の備忘録: "https://shizenkarasuzon.hatenablog.com/entry/2020/06/02/211314"
/// 
/// 
class ExampleWndProc extends Example {
    @override void excute() {
        // GetWindowTextLength(hWnd);
    }
}


/// 
/// SetForegroundWindow: "http://chokuto.ifdef.jp/urawaza/api/SetForegroundWindow.html"
/// 
class ExampleSetForegroundWindow extends Example {
    @override void excute() {
        printm('');
        printm('window handle で window を指定し、その window を最前面に配置し、active にする。');
        printm('成功した場合は 0 以外の値が返る。');
        printm('失敗した場合は 0 が返る。');
        printm('');
        
        printm('まずは、window handle を取得');
        final windowHandle = FindWindow(nullptr, TEXT('エクスプローラー'));
        printv('FindWindow(nullptr, TEXT("エクスプローラー"))', windowHandle);
        printm('');

        printm('window handle を指定し、SetForegroundWindow() を実行');
        printv('SetForegroundWindow($windowHandle)', SetForegroundWindow(windowHandle));
        printm('');
    }
}

/// 
/// 簡単な使用例がここにあった。
///     win32/sendinput.dart at main · timsneath/win32: "https://github.com/timsneath/win32/blob/main/example/sendinput.dart"
///     How to send keyboard events to a specified window？ · Issue #371 · timsneath/win32: "https://github.com/timsneath/win32/issues/371"
/// 
/// SendInput function - win32 library - Dart API: "https://pub.dev/documentation/win32/latest/win32/SendInput.html"
/// C# - SendInput(Windows API)によるキー入力のサンプル(32/64bit対応) - Qiita: "https://qiita.com/kob58im/items/ca3b61e72111dfbab8a8"
/// C#によるキーボード擬似操作について - C#～PG CENTER（プログラムセンター）～: "http://pgcenter.web.fc2.com/contents/csharp_sendinput.html"
/// SendInput : 料理人の備忘録: "http://busyoda.livedoor.blog/archives/6782070.html"
/// INPUT 構造体 : 料理人の備忘録: "http://busyoda.livedoor.blog/archives/6782113.html"
/// INPUT (winuser.h) - Win32 apps | Microsoft Docs: "https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-input"
/// SendInput function (winuser.h) - Win32 apps | Microsoft Docs: "https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-sendinput"
/// How to send keyboard events to a specified window？ · Issue #371 · timsneath/win32: "https://github.com/timsneath/win32/issues/371"
/// win32/sendinput.dart at main · timsneath/win32: "https://github.com/timsneath/win32/blob/main/example/sendinput.dart"
/// 
/// cInputs
///     pInputs で渡す構造体の配列の数。
/// 
/// pInputs
///     INPUT 構造体の配列。
/// 
/// cbSize
///     INPUT 構造体のバイトサイズ。
///     sizeOf function - dart:ffi library - Dart API: "https://api.dart.dev/stable/2.17.6/dart-ffi/sizeOf.html"
///     
/// 戻り値 正常に挿入されたイベントの数。
/// 
/// 
/// INPUT 構造体とは
/// INPUT class - win32 library - Dart API: "https://pub.dev/documentation/win32/latest/win32/INPUT-class.html"
/// key input、 mouse move、mouse click などのために、SendInput により利用される、それらの情報を格納するための構造体。
/// 
/// KEYBDINPUT class - win32 library - Dart API: "https://pub.dev/documentation/win32/latest/win32/KEYBDINPUT-class.html"
/// KEYBDINPUT (winuser.h) - Win32 apps | Microsoft Docs: "https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-keybdinput"
/// KEYBDINPUT 構造体 : 料理人の備忘録: "http://busyoda.livedoor.blog/archives/6782207.html"
/// 
/// 
class ExampleSendInput extends Example {
    @override void excute() {
        printm('');

        final hwnd = FindWindow(TEXT('notepad'), nullptr);
        SetForegroundWindow(hwnd);

        /// KEYBDINPUT (winuser.h) - Win32 apps | Microsoft Docs: "https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-keybdinput"
        /// KEYBDINPUT 構造体 : 料理人の備忘録: "http://busyoda.livedoor.blog/archives/6782207.html"
        /// SendInput : 料理人の備忘録: "http://busyoda.livedoor.blog/archives/6782070.html"
        /// キーボード入力について - Win32 apps | Microsoft Docs: "https://docs.microsoft.com/ja-jp/windows/win32/inputdev/about-keyboard-input"
        final keybdInputADown = calloc<KEYBDINPUT>();

        /// 仮想キーコードを設定
        keybdInputADown.ref.wVk = VK_A;

        /// ハードウェアスキャンコードを設定
        /// キーボード入力について - Win32 apps | Microsoft Docs: "https://docs.microsoft.com/ja-jp/windows/win32/inputdev/about-keyboard-input"
        ///     スキャンコードの説明欄にて
        ///     通常 application は scan code を無視して、仮想 key code から 押された key を判断する。scan code はデバイス依存の値が返ってくるため。
        ///     なので、ここでは、スキャンコードを無視してよい。
        keybdInputADown.ref.wScan = 0;

        /// いくつか設定する値がある(dw_flagsを参照)が、0 で、key down ということになるはず。
        /// key up したい場合、2になる。
        keybdInputADown.ref.dwFlags = 0;

        /// time stamp.
        /// 0 が指定されたら system 側が勝手に作る。
        keybdInputADown.ref.time = 0;

        /// key stroke に関するさらなる追加の情報。
        /// GetMessageExtraInfo　で取得できる。
        /// よくわからん。
        keybdInputADown.ref.dwExtraInfo = GetMessageExtraInfo();

        final keybdInputAUp = calloc<KEYBDINPUT>();
        keybdInputAUp.ref.wVk = VK_A;
        keybdInputAUp.ref.wScan = 0;
        keybdInputAUp.ref.dwFlags = KEYEVENTF_KEYUP;
        keybdInputAUp.ref.time = 0;
        keybdInputAUp.ref.dwExtraInfo = GetMessageExtraInfo();

        /// INPUT (winuser.h) - Win32 apps | Microsoft Docs: "https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-input"
        /// INPUT class - win32 library - Dart API: "https://pub.dev/documentation/win32/latest/win32/INPUT-class.html"
        final inputADown = calloc<INPUT>();

        /// INPUT (winuser.h) - Win32 apps | Microsoft Docs: "https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-input"
        /// INPUT_MOUSE, INPUT_KEYBOARD, INPUT_HARDWARE, のいずれかの値になる。
        inputADown.ref.type = INPUT_KEYBOARD;

        /// type で INPUT_KEYBOARD を選んだ場合、ki を選ぶ。
        /// そして、KEYBDINPUT が入る。
        inputADown.ref.ki = keybdInputADown.ref;
        
        final inputAUp = calloc<INPUT>();
        inputAUp.ref.type = INPUT_KEYBOARD;
        inputAUp.ref.ki = keybdInputAUp.ref;

        // final inputs = calloc<PointerArray<INPUT>>();
        // final inputs = calloc<INPUT>();
        // final array = wsalloc(wChars);
        // final array = wsalloc(2);
        // final ptr = array.cast<INPUT>();

        /// SendInput function (winuser.h) - Win32 apps | Microsoft Docs: "https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-sendinput"
        /// 本来　pInputs には、INPUT 構造体の配列が入るのだが、win32 package では、用意されてないっぽい。
        /// How to send keyboard events to a specified window？ · Issue #371 · timsneath/win32: "https://github.com/timsneath/win32/issues/371"
        SendInput(1, inputADown, sizeOf<INPUT>());
        SendInput(1, inputAUp, sizeOf<INPUT>());
        
        free(keybdInputADown);
        free(keybdInputAUp);
        
        free(inputADown);
        free(inputAUp);

        /// 短い記述の仕方
        final key = calloc<INPUT>();
        key.ref.type = INPUT_KEYBOARD;
        key.ref.ki.wVk = VK_B;
        final result = SendInput(1, key, sizeOf<INPUT>());
        if (result != TRUE) print('Error: ${GetLastError()}');
        free(key);

    }
}


/// 
/// window handle の性質
/// 
/// window は、その中身が変化(page の遷移、folder の移動、表示内容の変化)することにより、title(window title) は著しく変化する。
/// しかし、window title が変化しても、同様の window を対象としているなら、それの window handle は変化しない。
/// 
/// ある window が閉じ(application の終了)、また、開く(さっき終了した application を起動)と、
/// 終了前と起動後で、window handle は変化する。この時、前後で window title は同様の場合がある。
/// 
/// つまり、一度、window handle を取得すれば、その window が閉じなければ、
/// その window handle を利用し、その window を対象に取ることを何度も行うことができる。
/// しかし、window が閉じると、その window handle は対象を失い、利用できなくなる。
/// 同じ application だったとしても、再度作り出された window ならば、再度、window handle を取得しなおす必要がある。
/// 
/// ウィンドウハンドル ‐ 通信用語の基礎知識: "https://www.wdic.org/w/TECH/%E3%82%A6%E3%82%A3%E3%83%B3%E3%83%89%E3%82%A6%E3%83%8F%E3%83%B3%E3%83%89%E3%83%AB"
///     Windowsのプログラムを組む時には、画面上にあるウィンドウやオブジェクトを認識するため、この数値(ハンドル)を用いる。
///     APIとしては、CreateWindowなどでウィンドウやオブジェクトを作った時の返却値として、作られたものに対応するウィンドウハンドルが返る。以降、制御するにはこの値を用いることになる。
/// 
void windowHandle() {}

// 標準 Windows API: "http://wisdom.sakura.ne.jp/system/winapi/win32/index.html"
void main(List<String> args) {
    ExampleFindWindow();
    ExampleEnumWindows();
    ExampleSetForegroundWindow();
    // final hwnd = FindWindow(TEXT('notepad'), nullptr);
    // SetForegroundWindow(hwnd);
    ExampleSendInput();
}