
/// Virtual-Key Codes (Winuser.h) - Win32 apps | Microsoft Docs: "https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes"
///     仮想キーコードは、上記の microsoft のサイトにある。    
/// 
/// Dart Virtual Key Codes table and KeyboardObserver for win32 package: "https://gist.github.com/PlugFox/f233613aca7d5f998b53f52229d35b36#file-virtual_key_codes-dart"
///     下記の Virtual Key Code の一覧は上記の git から copy paste した。pub.dev には無かった。
/// 
/// win32 package に含まれていた 
///     win32/constants_nodoc.dart at main · timsneath/win32: "https://github.com/timsneath/win32/blob/main/lib/src/constants_nodoc.dart"
///     ただし、一部がない。A~Zなど。microsoft のサイトで、constatns 名が表示されていないからか？
/// 

/*
 * [V]irtual [K]ey codes table
 * Symbolic constant name = decimal value 1..255
 */

// /// Left mouse button
// const int VK_LBUTTON = 1;

// /// Right mouse button
// const int VK_RBUTTON = 2;

// /// Control-break processing
// const int VK_CANCEL = 3;

// /// Middle mouse button (three-button mouse)
// const int VK_MBUTTON = 4;

// /// Windows 2000: X1 mouse button
// const int VK_XBUTTON1 = 5;

// /// Windows 2000: X2 mouse button
// const int VK_XBUTTON2 = 6;

// /// BACKSPACE key
// const int VK_BACK = 8;

// /// TAB key
// const int VK_TAB = 9;

// /// CLEAR key
// const int VK_CLEAR = 12;

// /// ENTER key
// const int VK_RETURN = 13;

// /// SHIFT key
// const int VK_SHIFT = 16;

// /// CTRL key
// const int VK_CONTROL = 17;

// /// ALT key
// const int VK_MENU = 18;

// /// PAUSE key
// const int VK_PAUSE = 19;

// /// CAPS LOCK key
// const int VK_CAPITAL = 20;

// /// IME Kana mode
// const int VK_KANA = 21;

// /// IME Hanguel mode (maintained for compatibility; use VK_HANGUL)
// const int VK_HANGUEL = 21;

// /// IME Hangul mode
// const int VK_HANGUL = 21;

// /// IME Junja mode
// const int VK_JUNJA = 23;

// /// IME final mode
// const int VK_FINAL = 24;

// /// IME Hanja mode
// const int VK_HANJA = 25;

// /// IME Kanji mode
// const int VK_KANJI = 25;

// /// ESC key
// const int VK_ESCAPE = 27;

// /// IME convert
// const int VK_CONVERT = 28;

// /// IME nonconvert
// const int VK_NONCONVERT = 29;

// /// IME accept
// const int VK_ACCEPT = 30;

// /// IME mode change request
// const int VK_MODECHANGE = 31;

// /// SPACEBAR
// const int VK_SPACE = 32;

// /// PAGE UP key
// const int VK_PRIOR = 33;

// /// PAGE DOWN key
// const int VK_NEXT = 34;

// /// END key
// const int VK_END = 35;

// /// HOME key
// const int VK_HOME = 36;

// /// LEFT ARROW key
// const int VK_LEFT = 37;

// /// UP ARROW key
// const int VK_UP = 38;

// /// RIGHT ARROW key
// const int VK_RIGHT = 39;

// /// DOWN ARROW key
// const int VK_DOWN = 40;

// /// SELECT key
// const int VK_SELECT = 41;

// /// PRINT key
// const int VK_PRINT = 42;

/// EXECUTE key
// const int VK_EXECUTE = 43;

// /// PRINT SCREEN key
// const int VK_SNAPSHOT = 44;

// /// INS key
// const int VK_INSERT = 45;

// /// DEL key
// const int VK_DELETE = 46;

// /// HELP key
// const int VK_HELP = 47;

/// 0 key
const int VK_0 = 48;

/// 1 key
const int VK_1 = 49;

/// 2 key
const int VK_2 = 50;

/// 3 key
const int VK_3 = 51;

/// 4 key
const int VK_4 = 52;

/// 5 key
const int VK_5 = 53;

/// 6 key
const int VK_6 = 54;

/// 7 key
const int VK_7 = 55;

/// 8 key
const int VK_8 = 56;

/// 9 key
const int VK_9 = 57;

/// A key
const int VK_A = 65;

/// B key
const int VK_B = 66;

/// C key
const int VK_C = 67;

/// D key
const int VK_D = 68;

/// E key
const int VK_E = 69;

/// F key
const int VK_F = 70;

/// G key
const int VK_G = 71;

/// H key
const int VK_H = 72;

/// I key
const int VK_I = 73;

/// J key
const int VK_J = 74;

/// K key
const int VK_K = 75;

/// L key
const int VK_L = 76;

/// M key
const int VK_M = 77;

/// N key
const int VK_N = 78;

/// O key
const int VK_O = 79;

/// P key
const int VK_P = 80;

/// Q key
const int VK_Q = 81;

/// R key
const int VK_R = 82;

/// S key
const int VK_S = 83;

/// T key
const int VK_T = 84;

/// U key
const int VK_U = 85;

/// V key
const int VK_V = 86;

/// W key
const int VK_W = 87;

/// X key
const int VK_X = 88;

/// Y key
const int VK_Y = 89;

/// Z key
const int VK_Z = 90;

/// Left Windows key (Microsoft® Natural® keyboard)
const int VK_LWIN = 91;

/// Right Windows key (Natural keyboard)
const int VK_RWIN = 92;

/// Applications key (Natural keyboard)
const int VK_APPS = 93;

/// Computer Sleep key
const int VK_SLEEP = 95;

/// Numeric keypad 0 key
const int VK_NUMPAD0 = 96;

/// Numeric keypad 1 key
const int VK_NUMPAD1 = 97;

/// Numeric keypad 2 key
const int VK_NUMPAD2 = 98;

/// Numeric keypad 3 key
const int VK_NUMPAD3 = 99;

/// Numeric keypad 4 key
const int VK_NUMPAD4 = 100;

/// Numeric keypad 5 key
const int VK_NUMPAD5 = 101;

/// Numeric keypad 6 key
const int VK_NUMPAD6 = 102;

/// Numeric keypad 7 key
const int VK_NUMPAD7 = 103;

/// Numeric keypad 8 key
const int VK_NUMPAD8 = 104;

/// Numeric keypad 9 key
const int VK_NUMPAD9 = 105;

/// Multiply key
const int VK_MULTIPLY = 106;

/// Add key
const int VK_ADD = 107;

/// Separator key
const int VK_SEPARATOR = 108;

/// Subtract key
const int VK_SUBTRACT = 109;

/// Decimal key
const int VK_DECIMAL = 110;

/// Divide key
const int VK_DIVIDE = 111;

/// F1 key
const int VK_F1 = 112;

/// F2 key
const int VK_F2 = 113;

/// F3 key
const int VK_F3 = 114;

/// F4 key
const int VK_F4 = 115;

/// F5 key
const int VK_F5 = 116;

/// F6 key
const int VK_F6 = 117;

/// F7 key
const int VK_F7 = 118;

/// F8 key
const int VK_F8 = 119;

/// F9 key
const int VK_F9 = 120;

/// F10 key
const int VK_F10 = 121;

/// F11 key
const int VK_F11 = 122;

/// F12 key
const int VK_F12 = 123;

/// F13 key
const int VK_F13 = 124;

/// F14 key
const int VK_F14 = 125;

/// F15 key
const int VK_F15 = 126;

/// F16 key
const int VK_F16 = 127;

/// F17 key
const int VK_F17 = 128;

/// F18 key
const int VK_F18 = 129;

/// F19 key
const int VK_F19 = 130;

/// F20 key
const int VK_F20 = 131;

/// F21 key
const int VK_F21 = 132;

/// F22 key
const int VK_F22 = 133;

/// F23 key
const int VK_F23 = 134;

/// F24 key
const int VK_F24 = 135;

/// NUM LOCK key
const int VK_NUMLOCK = 144;

/// SCROLL LOCK key
const int VK_SCROLL = 145;

/// Left SHIFT key
const int VK_LSHIFT = 160;

/// Right SHIFT key
const int VK_RSHIFT = 161;

/// Left CONTROL key
const int VK_LCONTROL = 162;

/// Right CONTROL key
const int VK_RCONTROL = 163;

/// Left MENU key
const int VK_LMENU = 164;

/// Right MENU key
const int VK_RMENU = 165;

/// Windows 2000: Browser Back key
const int VK_BROWSER_BACK = 166;

/// Windows 2000: Browser Forward key
const int VK_BROWSER_FORWARD = 167;

/// Windows 2000: Browser Refresh key
const int VK_BROWSER_REFRESH = 168;

/// Windows 2000: Browser Stop key
const int VK_BROWSER_STOP = 169;

/// Windows 2000: Browser Search key
const int VK_BROWSER_SEARCH = 170;

/// Windows 2000: Browser Favorites key
const int VK_BROWSER_FAVORITES = 171;

/// Windows 2000: Browser Start and Home key
const int VK_BROWSER_HOME = 172;

/// Windows 2000: Volume Mute key
const int VK_VOLUME_MUTE = 173;

/// Windows 2000: Volume Down key
const int VK_VOLUME_DOWN = 174;

/// Windows 2000: Volume Up key
const int VK_VOLUME_UP = 175;

/// Windows 2000: Next Track key
const int VK_MEDIA_NEXT_TRACK = 176;

/// Windows 2000: Previous Track key
const int VK_MEDIA_PREV_TRACK = 177;

/// Windows 2000: Stop Media key
const int VK_MEDIA_STOP = 178;

/// Windows 2000: Play/Pause Media key
const int VK_MEDIA_PLAY_PAUSE = 179;

/// Windows 2000: Start Mail key
const int VK_LAUNCH_MAIL = 180;

/// Windows 2000: Select Media key
const int VK_LAUNCH_MEDIA_SELECT = 181;

/// Windows 2000: Start Application 1 key
const int VK_LAUNCH_APP1 = 182;

/// Windows 2000: Start Application 2 key
const int VK_LAUNCH_APP2 = 183;

/// Windows 2000: For the US standard keyboard, the ';:' key
const int VK_OEM_1 = 186;

/// Windows 2000: For any country/region, the '+' key
const int VK_OEM_PLUS = 187;

/// Windows 2000: For any country/region, the ',' key
const int VK_OEM_COMMA = 188;

/// Windows 2000: For any country/region, the '-' key
const int VK_OEM_MINUS = 189;

/// Windows 2000: For any country/region, the '.' key
const int VK_OEM_PERIOD = 190;

/// Windows 2000: For the US standard keyboard, the '/?' key
const int VK_OEM_2 = 191;

/// Windows 2000: For the US standard keyboard, the '`~' key
const int VK_OEM_3 = 192;

/// Windows 2000: For the US standard keyboard, the '[{' key
const int VK_OEM_4 = 219;

/// Windows 2000: For the US standard keyboard, the '\|' key
const int VK_OEM_5 = 220;

/// Windows 2000: For the US standard keyboard, the ']}' key
const int VK_OEM_6 = 221;

/// Windows 2000: For the US standard keyboard,
/// the 'single-quote/double-quote' key
const int VK_OEM_7 = 222;

///
const int VK_OEM_8 = 223;

/// Windows 2000: Either the angle bracket key
/// or the backslash key on the RT 102-key keyboard
const int VK_OEM_102 = 226;

/// Windows 95/98, Windows NT 4.0, Windows 2000: IME PROCESS key
const int VK_PROCESSKEY = 229;

/// // Windows 2000: Used to pass Unicode characters
/// as if they were keystrokes. The VK_PACKET key is
/// the low word of a 32-bit Virtual Key value used
/// for non-keyboard input methods. For more information,
/// see Remark in KEYBDINPUT, SendInput, WM_KEYDOWN, and WM_KEYUP
const int VK_PACKET = 231;

/// Attn key
const int VK_ATTN = 246;

/// CrSel key
const int VK_CRSEL = 247;

/// ExSel key
const int VK_EXSEL = 248;

/// Erase EOF key
const int VK_EREOF = 249;

/// Play key
const int VK_PLAY = 250;

/// Zoom key
const int VK_ZOOM = 251;

/// Reserved for future use
const int VK_NONAME = 252;

/// PA1 key
const int VK_PA1 = 253;

/// Clear key
const int VK_OEM_CLEAR = 254;