
/// KEYBDINPUT (winuser.h) - Win32 apps | Microsoft Docs: "https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-keybdinput"
/// KEYBDINPUT struct の dwFlags に利用される値とその意味。

/// If specified, the scan code was preceded by a prefix byte that has the value 0xE0 (224).
/// よくわからん。
/// ある範囲の scan code の場合に、指定が必要らしい。
///     KEYBDINPUT 構造体 : 料理人の備忘録: "http://busyoda.livedoor.blog/archives/6782207.html"
// const KEYEVENTF_EXTENDEDKEY = 0x0001;

/// If specified, the key is being released. If not specified, the key is being pressed.
/// key が離れた、つまり、key up したときに指定する値。
/// これが、指定されていない場合、key が押された、つまり、key down されたと判断される。
// const KEYEVENTF_KEYUP = 0x0002;

/// If specified, wScan identifies the key and wVk is ignored.
/// 指定すると wScan の値を参照して key を識別するようになり、wVk の値が無視されるようになる。
// const KEYEVENTF_SCANCODE = 0x0008;

/// If specified, the system synthesizes a VK_PACKET keystroke. The wVk parameter must be zero. This flag can only be combined with the KEYEVENTF_KEYUP flag. For more information, see the Remarks section.
/// よくわからん。
// const KEYEVENTF_UNICODE = 0x0004;

// win32 package に 含まれていた。
// win32/constants_nodoc.dart at main · timsneath/win32: "https://github.com/timsneath/win32/blob/main/lib/src/constants_nodoc.dart"
