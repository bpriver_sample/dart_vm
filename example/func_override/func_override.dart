

typedef Compare = int Function(Object a, Object b);

mixin SinglePattern<F extends Function> {
    Function get excute;
    Compare excuteA();
    F get value;
}

class A
    with
        SinglePattern<int Function(Object a, Object b)>
{
    @override
    // TODO: implement excute
    Function get excute => _excute;
    Function get excuteArg => _excuteArg;

    String _excute() {
        print('_excute');
        return '';
    }

    String _excuteArg(String arg) {
        print('_excute arg = $arg');
        return '';
    }
    
    @override
    Compare excuteA() {
        throw UnimplementedError();
    }
    
    @override
    final value = (Object a, Object b) {
        print('excute value a = $a, b = $b');
        return 0;
    };

}

void B() {
}

void main(List<String> args) {
    final instance = A();
    // instance.excute.call(); // excute _excute()
    // instance.excute(); // excute _excute()
    // instance.excuteArg('xxx'); // excute _excuteArg('xxx')
    // instance.excuteArg.call('xxx'); // excute _excuteArg('xxx')
    // instance.excuteArg.call(8); // error
    // instance.value('a', 'b'); // excute value a = a, b = b
}

