
import 'package:analyzer/dart/analysis/analysis_context.dart';
import 'package:analyzer/dart/analysis/context_builder.dart';

/// ContextBuilder class - context_builder library - Dart API: "https://pub.dev/documentation/analyzer/latest/dart_analysis_context_builder/ContextBuilder-class.html"
/// memory_file_system for detail.
void main(List<String> args) {
    final contextBuilder = ContextBuilder();
    // AnalysisContext class - analysis_context library - Dart API: "https://pub.dev/documentation/analyzer/latest/dart_analysis_analysis_context/AnalysisContext-class.html"
    // final AnalysisContext analysisContext = contextBuilder.createContext(contextRoot: contextRoot);
}