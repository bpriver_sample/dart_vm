
import 'package:analyzer/file_system/file_system.dart';

void main(List<String> args) {
    /// File class - file_system library - Dart API: "https://pub.dev/documentation/analyzer/latest/file_system_file_system/File-class.html"
    /// cant instance because abstract class.
    // final file = File();

    /// Folder class - file_system library - Dart API: "https://pub.dev/documentation/analyzer/latest/file_system_file_system/Folder-class.html"
    /// cant instance because abstract class.
    // final folder = Folder();

    /// Resource class - file_system library - Dart API: "https://pub.dev/documentation/analyzer/latest/file_system_file_system/Resource-class.html"
    /// cant instance because abstract class.
    /// Resource は File と Folder の抽象化
    // final resource = Resource();

    /// ResourceProvider class - file_system library - Dart API: "https://pub.dev/documentation/analyzer/latest/file_system_file_system/ResourceProvider-class.html"
    /// cant instance because abstract class.
    /// ResourceProvider Implementers
    ///     MemoryResourceProvider, OverlayResourceProvider, PhysicalResourceProvider,
    // final resourceProvider = ResourceProvider();
}