
import 'package:analyzer/dart/analysis/analysis_context.dart';
import 'package:analyzer/dart/analysis/context_builder.dart';
import 'package:analyzer/dart/analysis/context_locator.dart';
import 'package:analyzer/file_system/memory_file_system.dart';
import 'package:dart_vm/src/example.dart';
import 'package:path/path.dart' as path;

class MemoryFileSystemExample
    extends
        Example
{
    @override
    void excute() {
        /// Context class - path library - Dart API: "https://pub.dev/documentation/path/latest/path/Context-class.html"
        /// Creates a new path context for the given style and current directory.
        final context = path.Context();
        printv('context.current', context.current);
        printv('context.style', context.style);

        /// MemoryResourceProvider class - memory_file_system library - Dart API: "https://pub.dev/documentation/analyzer/latest/file_system_memory_file_system/MemoryResourceProvider-class.html"
        /// Implemented ResourceProvider
        final memoryResourceProvider = MemoryResourceProvider();
        printv('memoryResourceProvider.nextStamp', memoryResourceProvider.nextStamp);
        printv('memoryResourceProvider.pathContext', memoryResourceProvider.pathContext);
        printv('memoryResourceProvider.pathContext.current', memoryResourceProvider.pathContext.current);

        final givenContextMemoryResourceProvider = MemoryResourceProvider(context: context);
        printv('givenContextMemoryResourceProvider.nextStamp', givenContextMemoryResourceProvider.nextStamp);
        printv('givenContextMemoryResourceProvider.pathContext', givenContextMemoryResourceProvider.pathContext);
        printv('givenContextMemoryResourceProvider.pathContext.current', givenContextMemoryResourceProvider.pathContext.current);
        final absolutePath = context.dirname(context.current);
        printv('absolutePath', absolutePath);
        printv('givenContextMemoryResourceProvider.getResource(absolutePath)', givenContextMemoryResourceProvider.getResource(absolutePath));

        final contextLocator = ContextLocator(resourceProvider: givenContextMemoryResourceProvider);
        final exampleDartPath = path.join(context.current, 'lib', 'src', 'example.dart');
        final srcFolder = path.join(context.current, 'lib', 'src');
        final currentFolder = context.current;
        printv('exampleDartPath', exampleDartPath);
        printv('srcFolder', srcFolder);
        printv('currentFolder', currentFolder);
        final List<String> includedPaths = [
            exampleDartPath,
            // srcFolder,
            // currentFolder,
        ];
        printv('includedPaths', includedPaths);
        final rootContextList = contextLocator.locateRoots(includedPaths: includedPaths);
        printv('contextLocator.locateRoots(includedPaths: includedPaths)', rootContextList);
        printv('rootContextList[0]', rootContextList[0]);
        printv('rootContextList[0].included', rootContextList[0].included);

        final contextBuilder = ContextBuilder(resourceProvider: givenContextMemoryResourceProvider);
        final contextRoot = rootContextList[0];
        printv('contextRoot.included', contextRoot.included);
        printv('contextRoot.root', contextRoot.root);
        printv('contextRoot.resourceProvider.pathContext.current', contextRoot.resourceProvider.pathContext.current);
        printv('contextRoot.workspace.root', contextRoot.workspace.root);
        // final AnalysisContext analysisContext = contextBuilder.createContext(contextRoot: contextRoot);
        // printv('analysisContext', analysisContext);

        

    }
}
