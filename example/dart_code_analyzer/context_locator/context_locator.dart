
import 'package:analyzer/dart/analysis/analysis_context.dart';
import 'package:analyzer/dart/analysis/context_builder.dart';
import 'package:analyzer/dart/analysis/context_locator.dart';
import 'package:analyzer/dart/analysis/context_root.dart';
import 'package:analyzer/dart/analysis/results.dart';
import 'package:analyzer/dart/ast/ast.dart';
import 'package:analyzer/file_system/memory_file_system.dart';
import 'package:analyzer/file_system/overlay_file_system.dart';
import 'package:analyzer/file_system/physical_file_system.dart';
import 'package:analyzer/source/line_info.dart';
import 'package:dart_vm/dart_vm.dart';
import 'package:path/path.dart' as path;

/// ContextLocator class - context_locator library - Dart API: "https://pub.dev/documentation/analyzer/latest/dart_analysis_context_locator/ContextLocator-class.html"
class ContextLocatorExample
    extends
        Example
{
    @override
    void excute() async {
        final context = path.Context();
        final memoryResourceProvider = MemoryResourceProvider(context: context);
        final overlayResourceProvider = OverlayResourceProvider(memoryResourceProvider);
        final physicalResourceProvider = PhysicalResourceProvider(stateLocation: context.current);

        // final contextLocator = ContextLocator(resourceProvider: memoryResourceProvider);
        final contextLocator = ContextLocator(resourceProvider: physicalResourceProvider);

        final includedPaths = <String>[
            path.join(context.current, 'lib', 'src', 'example.dart'),
            path.join(context.current, 'lib', 'src'),
            context.current,
        ];

        final List<ContextRoot> contextRootList = contextLocator.locateRoots(includedPaths: includedPaths);
        printv('contextRootList[0].root', contextRootList[0].root);

        final contextBuilder = ContextBuilder(resourceProvider: physicalResourceProvider);
        final AnalysisContext analysisContext = contextBuilder.createContext(contextRoot: contextRootList[0]);
        printv('analysisContext.contextRoot', analysisContext.contextRoot);
        printv('analysisContext.currentSession', analysisContext.currentSession);
        printv('analysisContext.sdkRoot', analysisContext.sdkRoot);

        final contextRoot = analysisContext.contextRoot;
        final analysisSession = analysisContext.currentSession;
        final declaredVariables = analysisSession.declaredVariables;

        printv('declaredVariables.variableNames', declaredVariables.variableNames);
        printv('currentSession.declaredVariables', analysisSession.declaredVariables);
        // printv('currentSession.declaredVariables', currentSession.);

        printv('analysisSession.getFile(includedPaths[0])', analysisSession.getFile(includedPaths[0]));

        final someFileResult = analysisSession.getFile(includedPaths[0]);
        final fileResult = someFileResult as FileResult;
        printv('fileResult.isAugmentation', fileResult.isAugmentation);
        printv('fileResult.isLibrary', fileResult.isLibrary);
        printv('fileResult.isPart', fileResult.isPart);
        printv('fileResult.path', fileResult.path);
        printv('fileResult.uri', fileResult.uri);
        printv('fileResult.lineInfo', fileResult.lineInfo);
        // final LineInfo lineInfo = fileResult.lineInfo;
        printv('fileResult.session', fileResult.session);

        final SomeParsedLibraryResult someParsedLibraryResult = analysisSession.getParsedLibrary(includedPaths[0]);
        final ParsedLibraryResult parsedLibraryResult = someParsedLibraryResult as ParsedLibraryResult;
        printv('parsedLibraryResult.units', parsedLibraryResult.units);
        // printv('parsedLibraryResult.units', parsedLibraryResult.getElementDeclaration(element));

        final ParsedUnitResult parsedUnitResult = parsedLibraryResult.units[0];
        printv('parsedUnitResult.content', parsedUnitResult.content);
        printv('parsedUnitResult.unit', parsedUnitResult.unit);

        final CompilationUnit compilationUnit = parsedUnitResult.unit;
        printv('compilationUnit.declarations', compilationUnit.declarations);
        printv('compilationUnit.declarations.first', compilationUnit.declarations.first);
        printv('compilationUnit.declarations.first.childEntities', compilationUnit.declarations.first.childEntities);
        var i =0;
        for (var element in compilationUnit.declarations.first.childEntities) {
            printv('compilationUnit.declarations.first.childEntities[${i}]', element);
            printv('compilationUnit.declarations.first.childEntities[${i}]', element.runtimeType);
            if(element is MethodDeclaration) {
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody', element.body);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.isAsynchronous', element.body.isAsynchronous);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.isGenerator', element.body.isGenerator);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.isSynchronous', element.body.isSynchronous);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.keyword', element.body.keyword);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.star', element.body.star);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.beginToken', element.body.beginToken);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.childEntities', element.body.childEntities);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.childEntities.first', element.body.childEntities.first);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.childEntities.first.runtimeType', element.body.childEntities.first.runtimeType);
                final block = element.body.childEntities.first;
                if(block is Block) {
                    printv('block.statements', block.statements);
                    for (var statment in block.statements) {
                        printv('statement.runtimeType', statment.runtimeType);
                        printv('statement.childEntities', statment.childEntities);
                        for (var element in statment.childEntities) {
                            printv('runtimeType', element.runtimeType);
                            if(element is VariableDeclarationList) {
                                printv('VariableDeclarationList.childEntities', element.childEntities);
                                for (var element in element.childEntities) {
                                    printv('runtimeType', element.runtimeType);
                                    if(element is VariableDeclaration) {
                                        printv('VariableDeclaration.childEntities', element.childEntities);
                                        for (var element in element.childEntities) {
                                            printv('runtimeType', element.runtimeType);
                                            if(element is SimpleStringLiteral) {
                                                printv('SimpleStringLiteral.childEntities', element.childEntities);
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.end', element.body.end);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.endToken', element.body.endToken);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.isSynthetic', element.body.isSynthetic);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.parent', element.body.parent);
                printv('compilationUnit.declarations.first.childEntities[${i}]: FunctionBody.root', element.body.root);
            }
            i++;
        }
        // printv('compilationUnit.declarations.first.childEntities.toList()[13]', compilationUnit.declarations.first.childEntities.toList()[13]);
        // final MethodDeclaration methodDeclaration = compilationUnit.declarations.first.childEntities.toList()[13] as MethodDeclaration;
        // printv('methodDeclaration.body', methodDeclaration.body);

        printv('compilationUnit.declaredElement', compilationUnit.declaredElement);
        printv('compilationUnit.directives', compilationUnit.directives);
        printv('compilationUnit.featureSet', compilationUnit.featureSet);
        printv('compilationUnit.languageVersionToken', compilationUnit.languageVersionToken);
        printv('compilationUnit.lineInfo', compilationUnit.lineInfo);
        printv('compilationUnit.scriptTag', compilationUnit.scriptTag);
        printv('compilationUnit.sortedDirectivesAndDeclarations', compilationUnit.sortedDirectivesAndDeclarations);
        printv('compilationUnit.beginToken', compilationUnit.beginToken);
        printv('compilationUnit.childEntities', compilationUnit.childEntities);
        printv('compilationUnit.end', compilationUnit.end);
        printv('compilationUnit.endToken', compilationUnit.endToken);
        printv('compilationUnit.isSynthetic', compilationUnit.isSynthetic);
        printv('compilationUnit.length', compilationUnit.length);
        printv('compilationUnit.offset', compilationUnit.offset);
        printv('compilationUnit.parent', compilationUnit.parent);
        printv('compilationUnit.root', compilationUnit.root);
        printv('compilationUnit.getProperty(\'a\')', compilationUnit.getProperty('a'));

        final syntacticEntityList = compilationUnit.childEntities;
        for (var syntacticEntity in syntacticEntityList) {
            printv('syntacticEntity', syntacticEntity);
            printv('syntacticEntity.end', syntacticEntity.end);
            printv('syntacticEntity.length', syntacticEntity.length);
            printv('syntacticEntity.offset', syntacticEntity.offset);
        }


        final someUnitElementResult = await analysisSession.getUnitElement(includedPaths[0]);
        final unitElementResult = someUnitElementResult as UnitElementResult;
        printv('unitElementResult.element', unitElementResult.element);
        printv('unitElementResult.element.classes', unitElementResult.element.classes);
        printv('unitElementResult.element.classes.first.name', unitElementResult.element.classes.first.name);
        
        final classElement = unitElementResult.element.classes.first;
        printv('classElement', classElement);
        // printv('classElement.augmented', classElement.augmented);
        printv('classElement.allSupertypes', classElement.allSupertypes);
        // printv('classElement.augmentation', classElement.augmentation);
        printv('classElement.hasNonFinalField', classElement.hasNonFinalField);
        printv('classElement.hashCode', classElement.hashCode);
        printv('classElement.isAbstract', classElement.isAbstract);
        printv('classElement.isDartCoreEnum', classElement.isDartCoreEnum);
        printv('classElement.isDartCoreObject', classElement.isDartCoreObject);
        printv('classElement.isMixinApplication', classElement.isMixinApplication);
        printv('classElement.isValidMixin', classElement.isValidMixin);
        printv('classElement.runtimeType', classElement.runtimeType);
        printv('classElement.superclassConstraints', classElement.superclassConstraints);
        printv('classElement.supertype', classElement.supertype);
        printv('classElement.thisType', classElement.thisType);
        printv('classElement.unnamedConstructor', classElement.unnamedConstructor);
        printv('classElement.accessors', classElement.accessors);
        printv('classElement.constructors', classElement.constructors);
        printv('classElement.fields', classElement.fields);
        printv('classElement.fields.first', classElement.fields.first);
        printv('classElement.fields.first.context', classElement.fields.first.context);
        
        printv('classElement.fields.first.context.analysisOptions', classElement.fields.first.context.analysisOptions);
        printv('classElement.fields.first.context.analysisOptions', classElement.fields.first.context.analysisOptions);
        printv('classElement.fields.first.context.declaredVariables', classElement.fields.first.context.declaredVariables);
        printv('classElement.fields.first.context.sourceFactory', classElement.fields.first.context.sourceFactory);
        
        printv('classElement.fields.first.source', classElement.fields.first.source);
        printv('classElement.interfaces', classElement.interfaces);
        printv('classElement.methods', classElement.methods);
        printv('classElement.mixins', classElement.mixins);
        printv('classElement.context', classElement.context);
        printv('classElement.declaration', classElement.declaration);
        printv('classElement.displayName', classElement.displayName);
        printv('classElement.documentationComment', classElement.documentationComment);
        printv('classElement.enclosingElement3', classElement.enclosingElement3);
        printv('classElement.hasAlwaysThrows', classElement.hasAlwaysThrows);
        printv('classElement.hasDeprecated', classElement.hasDeprecated);
        printv('classElement.hasDoNotStore', classElement.hasDoNotStore);
        printv('classElement.hasFactory', classElement.hasFactory);
        printv('classElement.hasInternal', classElement.hasInternal);
        printv('classElement.hasIsTest', classElement.hasIsTest);
        printv('classElement.hasIsTestGroup', classElement.hasIsTestGroup);
        printv('classElement.hasJS', classElement.hasJS);
        printv('classElement.hasLiteral', classElement.hasLiteral);
        printv('classElement.hasMustBeOverridden', classElement.hasMustBeOverridden);
        printv('classElement.hasMustCallSuper', classElement.hasMustCallSuper);
        printv('classElement.hasNonVirtual', classElement.hasNonVirtual);
        printv('classElement.hasOptionalTypeArgs', classElement.hasOptionalTypeArgs);
        printv('classElement.hasOverride', classElement.hasOverride);
        printv('classElement.hasProtected', classElement.hasProtected);
        printv('classElement.hasRequired', classElement.hasRequired);
        printv('classElement.hasSealed', classElement.hasSealed);
        printv('classElement.hasUseResult', classElement.hasUseResult);
        printv('classElement.hasVisibleForOverriding', classElement.hasVisibleForOverriding);
        printv('classElement.hasVisibleForTemplate', classElement.hasVisibleForTemplate);
        printv('classElement.hasVisibleForTesting', classElement.hasVisibleForTesting);
        printv('classElement.id', classElement.id);
        printv('classElement.isPrivate', classElement.isPrivate);
        printv('classElement.isPublic', classElement.isPublic);
        printv('classElement.isSimplyBounded', classElement.isSimplyBounded);
        printv('classElement.isSynthetic', classElement.isSynthetic);
        printv('classElement.kind', classElement.kind);
        printv('classElement.library', classElement.library);
        printv('classElement.location', classElement.location);
        printv('classElement.metadata', classElement.metadata);
        printv('classElement.name', classElement.name);
        printv('classElement.nameLength', classElement.nameLength);
        printv('classElement.nameOffset', classElement.nameOffset);
        printv('classElement.nonSynthetic', classElement.nonSynthetic);
        printv('classElement.session', classElement.session);
        printv('classElement.source', classElement.source);
        printv('classElement.typeParameters', classElement.typeParameters);
        printv('classElement.librarySource', classElement.librarySource);
        // printv('classElement.librarySource', classElement.accept(visitor));
        printv('classElement.getDisplayString(withNullability: true)', classElement.getDisplayString(withNullability: true));
        printv('classElement.getDisplayString(withNullability: false)', classElement.getDisplayString(withNullability: false));
        printv('classElement.getExtendedDisplayName(null)', classElement.getExtendedDisplayName(null));
        printv('classElement.getField(\'a\')', classElement.getField('a'));
        // printv('classElement.getField(\'a\')', classElement.visitChildren(visitor));

        final classElementContext = classElement.context;
        printv('classElementContext.analysisOptions', classElementContext.analysisOptions);
        printv('classElementContext.declaredVariables', classElementContext.declaredVariables);
        printv('classElementContext.sourceFactory', classElementContext.sourceFactory);

        final methodElements = classElement.methods;
        for (var methodElement in methodElements) {
            // printv('methodElement.augmentation', methodElement.augmentation);
            printv('methodElement', methodElement);
            printv('methodElement.declaration', methodElement.declaration);
            printv('methodElement.displayName', methodElement.displayName);
            printv('methodElement.enclosingElement3', methodElement.enclosingElement3);
            printv('methodElement.hasImplicitReturnType', methodElement.hasImplicitReturnType);
            printv('methodElement.isAbstract', methodElement.isAbstract);
            printv('methodElement.isAsynchronous', methodElement.isAsynchronous);
            printv('methodElement.isExternal', methodElement.isExternal);
            printv('methodElement.isGenerator', methodElement.isGenerator);
            printv('methodElement.isOperator', methodElement.isOperator);
            printv('methodElement.isStatic', methodElement.isStatic);
            printv('methodElement.isSynchronous', methodElement.isSynchronous);
            printv('methodElement.name', methodElement.name);
            printv('methodElement.runtimeType', methodElement.runtimeType);
            printv('methodElement.context', methodElement.context);
            printv('methodElement.documentationComment', methodElement.documentationComment);
            printv('methodElement.hasAlwaysThrows', methodElement.hasAlwaysThrows);
            printv('methodElement.hasDeprecated', methodElement.hasDeprecated);
            printv('methodElement.hasDoNotStore', methodElement.hasDoNotStore);
            printv('methodElement.hasFactory', methodElement.hasFactory);
            printv('methodElement.hasInternal', methodElement.hasInternal);
            printv('methodElement.hasIsTest', methodElement.hasIsTest);
            printv('methodElement.parameters', methodElement.parameters);
            printv('methodElement.returnType', methodElement.returnType);
            printv('methodElement.session', methodElement.session);
            printv('methodElement.type', methodElement.type);
            printv('methodElement.typeParameters', methodElement.typeParameters);
            
            printv('methodElement.hashCode', methodElement.hashCode);
            printv('methodElement.isAbstract', methodElement.isAbstract);
            printv('methodElement.runtimeType', methodElement.runtimeType);
            printv('methodElement.context', methodElement.context);
            printv('methodElement.displayName', methodElement.displayName);
            printv('methodElement.documentationComment', methodElement.documentationComment);
            printv('methodElement.enclosingElement3', methodElement.enclosingElement3);
            printv('methodElement.hasAlwaysThrows', methodElement.hasAlwaysThrows);
            printv('methodElement.hasDeprecated', methodElement.hasDeprecated);
            printv('methodElement.hasDoNotStore', methodElement.hasDoNotStore);
            printv('methodElement.hasFactory', methodElement.hasFactory);
            printv('methodElement.hasInternal', methodElement.hasInternal);
            printv('methodElement.hasIsTest', methodElement.hasIsTest);
            printv('methodElement.hasIsTestGroup', methodElement.hasIsTestGroup);
            printv('methodElement.hasJS', methodElement.hasJS);
            printv('methodElement.hasLiteral', methodElement.hasLiteral);
            printv('methodElement.hasMustBeOverridden', methodElement.hasMustBeOverridden);
            printv('methodElement.hasMustCallSuper', methodElement.hasMustCallSuper);
            printv('methodElement.hasNonVirtual', methodElement.hasNonVirtual);
            printv('methodElement.hasOptionalTypeArgs', methodElement.hasOptionalTypeArgs);
            printv('methodElement.hasOverride', methodElement.hasOverride);
            printv('methodElement.hasProtected', methodElement.hasProtected);
            printv('methodElement.hasRequired', methodElement.hasRequired);
            printv('methodElement.hasSealed', methodElement.hasSealed);
            printv('methodElement.hasUseResult', methodElement.hasUseResult);
            printv('methodElement.hasVisibleForOverriding', methodElement.hasVisibleForOverriding);
            printv('methodElement.hasVisibleForTemplate', methodElement.hasVisibleForTemplate);
            printv('methodElement.hasVisibleForTesting', methodElement.hasVisibleForTesting);
            printv('methodElement.id', methodElement.id);
            printv('methodElement.isPrivate', methodElement.isPrivate);
            printv('methodElement.isPublic', methodElement.isPublic);
            printv('methodElement.isSimplyBounded', methodElement.isSimplyBounded);
            printv('methodElement.isSynthetic', methodElement.isSynthetic);
            printv('methodElement.kind', methodElement.kind);
            printv('methodElement.library', methodElement.library);
            printv('methodElement.location', methodElement.location);
            printv('methodElement.metadata', methodElement.metadata);
            printv('methodElement.name', methodElement.name);
            printv('methodElement.nameLength', methodElement.nameLength);
            printv('methodElement.nameOffset', methodElement.nameOffset);
            printv('methodElement.nonSynthetic', methodElement.nonSynthetic);
            printv('methodElement.session', methodElement.session);
            printv('methodElement.source', methodElement.source);
            printv('methodElement.typeParameters', methodElement.typeParameters);
            printv('methodElement.librarySource', methodElement.librarySource);
            printv('methodElement.getDisplayString(withNullability: true)', methodElement.getDisplayString(withNullability: true));
            printv('methodElement.getDisplayString(withNullability: false)', methodElement.getDisplayString(withNullability: false));
            printv('methodElement.getExtendedDisplayName(null)', methodElement.getExtendedDisplayName(null));          
        }



        final functionElementList = unitElementResult.element.functions;
        printv('functionElementList', functionElementList);

        final constructorElementList = unitElementResult.element.classes.first.constructors;
        for (var constructorElement in constructorElementList) {
            printv('constructorElement', constructorElement.context);
            printv('constructorElement.context.declaredVariables.variableNames', constructorElement.context.declaredVariables.variableNames);
        }

        // analysisSession.getParsedLibraryByElement(classElement);
        // analysisSession.getUnitElement(classElement);
        // analysisSession.getResolvedUnit(classElement);


        // utilities library - Dart API: "https://pub.dev/documentation/analyzer/latest/dart_analysis_utilities/dart_analysis_utilities-library.html"

    }
}
