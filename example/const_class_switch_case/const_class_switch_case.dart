
abstract class ParentClass { const ParentClass(); String get value;}
class NotConstClass extends ParentClass {
    NotConstClass();
    @override final String value = 'not const';
}
// omit 省略。
class OmitConstClass extends ParentClass {
    @override final String value = 'omit const';
}
class ConstClassA extends ParentClass {
    const ConstClassA();
    @override
    final value = 'A';
}
class ConstClassB extends ParentClass {
    const ConstClassB(this.value);
    @override
    final String value;
}
class ConstClassC extends ParentClass {
    const ConstClassC();
    @override
    final value = 'C';
    @override
    bool operator ==(dynamic other) {
        if( other.runtimeType != runtimeType ) return false;
        var typeChecked = other as ConstClassC;
        return value == typeChecked.value;
    }
    @override
    int get hashCode {
        var result = 17;
        result = 37 * result + toString().hashCode;
        return result;
    }
    @override
    String toString() {
        return value;
    }
}


String switchCaseInstance() {
    // final ParentClass expression = ConstClassA(); // return default. because ...?
    final ParentClass expression = ConstClassB('abc'); // return default. because ...?
    switch (expression) {
        case ConstClassA(): return 'case 1'; // OK. because const.
        case ConstClassB('abc'): return 'case 2'; // OK. because const.
        // case ConstClassA().valueA: return 'case 3'; // Error. because not const.
        // case NotConstClass(): return 'case 4'; // Error. because not const.
        // case ConstClassC(): return 'case 5'; // Error. because = operator override. Diagnostic messages | Dart: "https://dart.dev/tools/diagnostic-messages?utm_source=dartdev&utm_medium=redir&utm_id=diagcode&utm_content=case_expression_type_implements_equals#case_expression_type_implements_equals"
        default: return 'case default';
    }
}

String switchCaseRuntimeType() {
    // final ParentClass expression = ConstClassA(); // return case 1.
    // final ParentClass expression = ConstClassB('a'); // return case 2.
    // final ParentClass expression = ConstClassC(); // return case 3.
    final ParentClass expression = NotConstClass(); // return case last.
    switch (expression.runtimeType) {
        case ConstClassA: return 'case 1'; // OK. because const.
        case ConstClassB: return 'case 2'; // OK. because const.
        case ConstClassC: return 'case 3'; // Error. because = operator override. Diagnostic messages | Dart: "https://dart.dev/tools/diagnostic-messages?utm_source=dartdev&utm_medium=redir&utm_id=diagcode&utm_content=case_expression_type_implements_equals#case_expression_type_implements_equals"
        case NotConstClass: return 'case last'; // Error. because not const.
        default: return 'case default';
    }
}

String switchCaseInstanceValue() {
    final expression = 'A'; // return case 1.
    switch (expression) {
        // case ConstClassA().value: return 'case 1'; // Error. because not const.
        // default: return 'case default';
    }
    if (expression == ConstClassA().value) return 'case 1';
    return 'default';
}

void main(List<String> args) {
    
    print(switchCaseInstance());
    print(switchCaseRuntimeType());
    print(switchCaseInstanceValue());

    final a = const ConstClassA();
    // final omit = const OmitConstClass(); // Error. not const.

}